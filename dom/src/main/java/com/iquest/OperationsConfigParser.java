package com.iquest;

import com.iquest.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

import static com.iquest.constants.ParserConstants.*;

/**
 * @author Laura Mamina
 */
public class OperationsConfigParser {

  public OperationsConfig parse(String fileName, String schemaFileName) throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setValidating(true);
    documentBuilderFactory.setNamespaceAware(true);
    documentBuilderFactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
            "http://www.w3.org/2001/XMLSchema");

    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    documentBuilder.setErrorHandler(new OperationsConfigErrorHandler());
    Document doc = documentBuilder.parse(fileName);
    doc.normalize();
    doc.normalizeDocument();

    return processParsing(doc);
  }

  private OperationsConfig processParsing(Document doc) {
    Node operationsConfigNode = doc.getFirstChild();
    OperationsConfig operationsConfig = new OperationsConfig();

    NodeList childNodes = operationsConfigNode.getChildNodes();
    int childNodesLength = childNodes.getLength();
    for (int i = 0; i < childNodesLength; i++) {
      Node child = childNodes.item(i);
      if (!(child.getNodeType() == Node.ELEMENT_NODE)) {
        continue;
      }
      switch (child.getNodeName()) {
        case DEFAULT_DATA: {
          DefaultData defaultData = new DefaultData();
          operationsConfig.setDefaultData(defaultData);
          processDefaultData(child, defaultData);
          break;
        }
        case RELOAD: {
          ReloadSpecificMock reloadMock = new ReloadSpecificMock();
          operationsConfig.addReloadSpecificMock(reloadMock);
          processReloadSpecific(child, reloadMock);
          break;
        }
      }
    }
    return operationsConfig;
  }

  private void processDefaultData(Node defaultDataNode, DefaultData defaultData) {
    NodeList childNodes = defaultDataNode.getChildNodes();
    int childNodesLength = childNodes.getLength();
    for (int i = 0; i < childNodesLength; i++) {
      Node child = childNodes.item(i);
      if (!(child.getNodeType() == Node.ELEMENT_NODE)) {
        continue;
      }
      switch (child.getNodeName()) {
        case IS_ALIVE: {
          IsAliveMock isAliveMock = new IsAliveMock();
          defaultData.setAliveMock(isAliveMock);
          processDefaultIsAlive(child, isAliveMock);
          break;
        }
        case RELOAD: {
          ReloadMock reloadMock = new ReloadMock();
          defaultData.setReloadMock(reloadMock);
          processDefaultReload(child, reloadMock);
          break;
        }
      }
    }
  }

  private void processDefaultIsAlive(Node isAliveNode, IsAliveMock isAliveMock) {
    NodeList childNodes = isAliveNode.getChildNodes();
    int childNodesLength = childNodes.getLength();
    for (int i = 0; i < childNodesLength; i++) {
      Node child = childNodes.item(i);
      if (!(child.getNodeType() == Node.ELEMENT_NODE)) {
        continue;
      }
      switch (child.getNodeName()) {
        case RESPONSE: {
          isAliveMock.setResponse(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
        case SLEEP_TIME: {
          isAliveMock.setSleepTime(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
      }
    }
  }

  private void processDefaultReload(Node defaultReloadNode, ReloadMock reloadMock) {
    NodeList childNodes = defaultReloadNode.getChildNodes();
    int childNodesLength = childNodes.getLength();
    for (int i = 0; i < childNodesLength; i++) {
      Node child = childNodes.item(i);
      if (!(child.getNodeType() == Node.ELEMENT_NODE)) {
        continue;
      }
      switch (child.getNodeName()) {
        case RESULT_CODE: {
          reloadMock.setResultCode(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
        case RESULT_TEXT: {
          reloadMock.setResultText(child.getFirstChild().getNodeValue());
          break;
        }
        case SLEEP_TIME: {
          reloadMock.setSleepTime(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
      }
    }
  }

  private void processReloadSpecific(Node specificReloadNode, ReloadSpecificMock reloadMock) {
    NodeList childNodes = specificReloadNode.getChildNodes();
    int childNodesLength = childNodes.getLength();
    for (int i = 0; i < childNodesLength; i++) {
      Node child = childNodes.item(i);
      if (!(child.getNodeType() == Node.ELEMENT_NODE)) {
        continue;
      }
      switch (child.getNodeName()) {
        case RESULT_CODE: {
          reloadMock.setResultCode(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
        case RESULT_TEXT: {
          reloadMock.setResultText(child.getFirstChild().getNodeValue());
          break;
        }
        case SLEEP_TIME: {
          reloadMock.setSleepTime(Integer.parseInt(child.getFirstChild().getNodeValue()));
          break;
        }
        case MSISDN: {
          reloadMock.setMsisdn(child.getFirstChild().getNodeValue());
          break;
        }
      }
    }
  }
}