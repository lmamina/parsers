package com.iquest;


import com.iquest.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import static com.iquest.constants.ParserConstants.*;

/**
 * @author Laura Mamina
 */
public class OperationsConfigHandler extends DefaultHandler {

  private OperationsConfig operationsConfig;
  private boolean isIntoDefaultData = false;
  private boolean isIntoIsAlive = false;
  private boolean isIntoReload = false;
  private String lastLevel = "";

  public OperationsConfig getOperationsConfig() {
    return operationsConfig;
  }

  @Override
  public void startDocument() {
    operationsConfig = new OperationsConfig();
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) {
    switch (qName) {
      case DEFAULT_DATA: {
        operationsConfig.setDefaultData(new DefaultData());
        isIntoDefaultData = true;
        break;
      }
      case IS_ALIVE: {
        IsAliveMock isAliveMock = new IsAliveMock();
        operationsConfig.getDefaultData().setAliveMock(isAliveMock);
        isIntoIsAlive = true;
        break;
      }
      case RELOAD: {
        if (isIntoDefaultData) {
          operationsConfig.getDefaultData().setReloadMock(new ReloadMock());
        } else {
          operationsConfig.getReloadSpecificMocks().add(new ReloadSpecificMock());
        }
        isIntoReload = true;
        break;
      }
      default: {
        lastLevel = qName;
        break;
      }
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException {
    if (isIntoIsAlive) {
      IsAliveMock isAliveMock = operationsConfig.getDefaultData().getAliveMock();
      storeIsAliveData(isAliveMock, new String(ch, start, length));
    } else if (isIntoReload && isIntoDefaultData) {
      ReloadMock reloadMock = operationsConfig.getDefaultData().getReloadMock();
      storeBaseReload(reloadMock, new String(ch, start, length));
    } else if (isIntoReload) {
      ReloadSpecificMock reloadSpecificMock = operationsConfig.getLastReloadSpecificMock();
      storeSpecificReload(reloadSpecificMock, new String(ch, start, length));
    }
    lastLevel = "";
  }

  private void storeIsAliveData(IsAliveMock isAliveMock, String ch) {
    switch (lastLevel) {
      case RESPONSE: {
        isAliveMock.setResponse(Integer.parseInt(ch));
        break;
      }
      case SLEEP_TIME: {
        isAliveMock.setSleepTime(Integer.parseInt(ch));
        break;
      }
    }
  }

  private void storeBaseReload(ReloadMock reloadMock, String ch) {
    switch (lastLevel) {
      case RESULT_CODE: {
        reloadMock.setResultCode(Integer.parseInt(ch));
        break;
      }
      case RESULT_TEXT: {
        reloadMock.setResultText(ch);
        break;
      }
      case SLEEP_TIME: {
        reloadMock.setSleepTime(Integer.parseInt(ch));
        break;
      }
    }
  }

  private void storeSpecificReload(ReloadSpecificMock reloadMock, String ch) {
    if (lastLevel.equals(MSISDN)) {
      reloadMock.setMsisdn(ch);
    } else {
      storeBaseReload(reloadMock, ch);
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName) {
      case DEFAULT_DATA: {
        isIntoDefaultData = false;
        break;
      }
      case IS_ALIVE: {
        isIntoIsAlive = false;
        break;
      }
      case RELOAD: {
        isIntoReload = false;
        break;
      }
    }
  }
}
