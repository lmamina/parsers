package com.iquest;

import com.iquest.model.OperationsConfig;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;

/**
 * @author Laura Mamina
 */
public class OperationsConfigParser {

  public OperationsConfig parse(String fileName, String schemaFileName) throws ParserConfigurationException, SAXException, IOException {
    SAXParserFactory factory = SAXParserFactory.newInstance();

    factory.setValidating(true);
    factory.setNamespaceAware(true);
    SAXParser saxParser = factory.newSAXParser();
    saxParser.setProperty("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
            "http://www.w3.org/2001/XMLSchema");


    XMLReader reader = saxParser.getXMLReader();
    reader.setErrorHandler(new OperationsConfigErrorHandler());
    reader.parse(new InputSource(fileName));

    OperationsConfigHandler operationsConfigHandler = new OperationsConfigHandler();
    saxParser.parse(fileName, operationsConfigHandler);
    return operationsConfigHandler.getOperationsConfig();
  }

}