package com.iquest;

import com.iquest.model.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;

import static com.iquest.constants.ParserConstants.*;

/**
 * @author Laura Mamina
 */
public class OperationsConfigParser {

  private OperationsConfig operationsConfig;
  private boolean isIntoDefaultData = false;
  private boolean isIntoIsAlive = false;
  private boolean isIntoReload = false;
  private String lastLevel = "";

  public OperationsConfig parse(String fileName, String schemaFile) throws XMLStreamException, IOException, SAXException {
    Reader re = new FileReader(fileName);

    SchemaFactory factory =
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema schema = factory.newSchema(new File(schemaFile));

    Validator validator = schema.newValidator();
    validator.validate(new StreamSource(re));

    XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
    Reader r = new FileReader(fileName);

    XMLStreamReader reader = xmlFactory.createXMLStreamReader(r);

    operationsConfig = new OperationsConfig();

    while (reader.hasNext()) {
      int event = reader.next();
      switch (event) {
        case XMLStreamConstants.START_ELEMENT: {
          doStartElement(reader.getLocalName());
          break;
        }
        case XMLStreamConstants.CHARACTERS: {
          doCharacters(reader.getText());
          break;
        }
        case XMLStreamConstants.END_ELEMENT: {
          doEndElement(reader.getLocalName());
          break;
        }
      }
    }
    return operationsConfig;
  }

  private void doStartElement(String localName) {
    switch (localName) {
      case DEFAULT_DATA: {
        operationsConfig.setDefaultData(new DefaultData());
        isIntoDefaultData = true;
        break;
      }
      case IS_ALIVE: {
        IsAliveMock isAliveMock = new IsAliveMock();
        operationsConfig.getDefaultData().setAliveMock(isAliveMock);
        isIntoIsAlive = true;
        break;
      }
      case RELOAD: {
        if (isIntoDefaultData) {
          operationsConfig.getDefaultData().setReloadMock(new ReloadMock());
        } else {
          operationsConfig.getReloadSpecificMocks().add(new ReloadSpecificMock());
        }
        isIntoReload = true;
        break;
      }
      default: {
        lastLevel = localName;
        break;
      }
    }
  }

  private void doCharacters(String text) {
    if (isIntoIsAlive) {
      IsAliveMock isAliveMock = operationsConfig.getDefaultData().getAliveMock();
      storeIsAliveData(isAliveMock, text);
    } else if (isIntoReload && isIntoDefaultData) {
      ReloadMock reloadMock = operationsConfig.getDefaultData().getReloadMock();
      storeBaseReload(reloadMock, text);
    } else if (isIntoReload) {
      ReloadSpecificMock reloadSpecificMock = operationsConfig.getLastReloadSpecificMock();
      storeSpecificReload(reloadSpecificMock, text);
    }
    lastLevel = "";
  }

  private void storeIsAliveData(IsAliveMock isAliveMock, String ch) {
    switch (lastLevel) {
      case RESPONSE: {
        isAliveMock.setResponse(Integer.parseInt(ch));
        break;
      }
      case SLEEP_TIME: {
        isAliveMock.setSleepTime(Integer.parseInt(ch));
        break;
      }
    }
  }

  private void storeBaseReload(ReloadMock reloadMock, String ch) {
    switch (lastLevel) {
      case RESULT_CODE: {
        reloadMock.setResultCode(Integer.parseInt(ch));
        break;
      }
      case RESULT_TEXT: {
        reloadMock.setResultText(ch);
        break;
      }
      case SLEEP_TIME: {
        reloadMock.setSleepTime(Integer.parseInt(ch));
        break;
      }
    }
  }

  private void storeSpecificReload(ReloadSpecificMock reloadMock, String ch) {
    if (lastLevel.equals(MSISDN)) {
      reloadMock.setMsisdn(ch);
    } else {
      storeBaseReload(reloadMock, ch);
    }
  }

  private void doEndElement(String localName) {
    switch (localName) {
      case DEFAULT_DATA: {
        isIntoDefaultData = false;
        break;
      }
      case IS_ALIVE: {
        isIntoIsAlive = false;
        break;
      }
      case RELOAD: {
        isIntoReload = false;
        break;
      }
    }
  }
}