package com.iquest;

import com.iquest.model.OperationsConfig;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Laura Mamina
 */
public class OperationsConfigParserTest {

  @Test
  public void testParsingFile() throws XMLStreamException, IOException, SAXException {
    OperationsConfigParser parser = new OperationsConfigParser();
    OperationsConfig operationsConfig = parser.parse("src/test/resources/operations-config.xml", "src/test/resources/operations-config.xsd");
    return;
  }
}
