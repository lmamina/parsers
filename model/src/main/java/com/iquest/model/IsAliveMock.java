package com.iquest.model;

/**
 * @author Laura Mamina
 */
public class IsAliveMock {

  private int response;
  private int sleepTime;

  public IsAliveMock() {
  }

  public int getResponse() {
    return response;
  }

  public void setResponse(int response) {
    this.response = response;
  }

  public int getSleepTime() {
    return sleepTime;
  }

  public void setSleepTime(int sleepTime) {
    this.sleepTime = sleepTime;
  }
}
