package com.iquest.model;

/**
 * @author Laura Mamina
 */
public class ReloadMock {

  private int resultCode;
  private String resultText;
  private int sleepTime;

  public ReloadMock() {
  }

  public int getResultCode() {
    return resultCode;
  }

  public void setResultCode(int resultCode) {
    this.resultCode = resultCode;
  }

  public String getResultText() {
    return resultText;
  }

  public void setResultText(String resultText) {
    this.resultText = resultText;
  }

  public int getSleepTime() {
    return sleepTime;
  }

  public void setSleepTime(int sleepTime) {
    this.sleepTime = sleepTime;
  }
}
