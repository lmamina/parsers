package com.iquest.model;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

/**
 * @author Laura Mamina
 */
public class OperationsConfig {

  private DefaultData defaultData;
  private Deque<ReloadSpecificMock> reloadSpecificMocks = new LinkedList<>();

  public DefaultData getDefaultData() {
    return defaultData;
  }

  public void setDefaultData(DefaultData defaultData) {
    this.defaultData = defaultData;
  }

  public Collection<ReloadSpecificMock> getReloadSpecificMocks() {
    return reloadSpecificMocks;
  }

  public ReloadSpecificMock getLastReloadSpecificMock() {
    if (reloadSpecificMocks.size() == 0) {
      throw new ArrayIndexOutOfBoundsException("There is no reload specific mock stored");
    }
    return reloadSpecificMocks.getLast();
  }

  public void addReloadSpecificMock(ReloadSpecificMock reloadSpecificMock) {
    reloadSpecificMocks.addLast(reloadSpecificMock);
  }
}
