package com.iquest.model;

/**
 * @author Laura Mamina
 */
public class ReloadSpecificMock extends ReloadMock{

  private String msisdn;

  public ReloadSpecificMock() {
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
}
