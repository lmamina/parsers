package com.iquest.model;

/**
 * @author Laura Mamina
 */
public class DefaultData {

  private IsAliveMock isAliveMock;
  private ReloadMock reloadMock;

  public DefaultData() {
  }

  public IsAliveMock getAliveMock() {
    return isAliveMock;
  }

  public void setAliveMock(IsAliveMock aliveMock) {
    isAliveMock = aliveMock;
  }

  public ReloadMock getReloadMock() {
    return reloadMock;
  }

  public void setReloadMock(ReloadMock reloadMock) {
    this.reloadMock = reloadMock;
  }
}
