package com.iquest.constants;

/**
 * @author Laura Mamina
 */
public class ParserConstants {
  public static final String DEFAULT_DATA = "default-data";
  public static final String RELOAD = "reload";
  public static final String IS_ALIVE = "is-alive";
  public static final String RESPONSE = "response";
  public static final String SLEEP_TIME = "sleep-time";
  public static final String RESULT_CODE = "result-code";
  public static final String RESULT_TEXT = "result-text";
  public static final String MSISDN = "msisdn";
}
